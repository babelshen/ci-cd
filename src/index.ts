import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import { getTimestamp } from './utils';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.get('/', (request: Request, response: Response) => {
  response.send(`Express + Typescript server. Timestamp: ${getTimestamp()}`);
});

app.listen(port, () => {
  console.log(`Server is running at port ${port}`);
});
