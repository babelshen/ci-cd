import { getTimestamp } from '../src/utils';

describe('getTimestamp', () => {
  it('return a number', () => {
    const timestamp = getTimestamp();
    expect(typeof timestamp).toBe('number');
  });
});